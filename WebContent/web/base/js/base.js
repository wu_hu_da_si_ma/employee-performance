var form = layui.form;
var $ = layui.jquery;
var layer = layui.layer;
var element = layui.element;
var laypage=layui.laypage;
var upload =layui.upload;

function formSubmit(url,submit,dataType,func){
form.on(submit,function(data){
	ajax(url,data.field,dataType,func);
})
}

function ajax(url,field,dataType,func){
$.ajax({
	url:base.app+url,
	type:"post",
	data:field,
	dataType:dataType,
	success:func
})
}

function openLayer(url){
	layer.open({
		type:2,
		area:['750px','600px'],
		fixed:false,
		maxmin:true,
		content:base.app+url
	})
}	
function openConfirm(func,title){
	layer.confirm(title?title:"是否执行该操作",{icon:3,title:"提示"},func)
}
function randerUpload(id,url,data,done){
	upload.render({//
		elem:id,
		url:url,
		data:data,
		done:done
	})
}
function setPageInfo(elem,count,curr,limit,jump){
	laypage.render({
		elem:elem,
		count:count,//总数
		curr:curr,//默认位置
		limit:limit,
		limits:[10,20,30,40,50],//选择框
		layout:['count','prev','page','next','limit','refresh','skip'],
		jump:jump
	})
	//当分页被切换是触发jump，该方法有两个参数
	//obj=当前分页所有选项的值
	//first=第一次执行，一般用于初始加载时判断
}