<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="/web/header.jsp" %>
<title>用户注册</title>
</head>
<body style = "background:#D8CEF6">
<h1 style="text-align:center;font-weight:bold;">欢迎注册员工绩效管理系统！</h1>
<div class="window">
<fieldset class="layui-elem-field" style="marhin:10px">
	<legend>注册</legend>
<form class="layui-form "  onsubmit="return false">
<div class="layui-form-item">
		<label class="layui-form-label">账号</label>
			<div class="layui-input-inline">
				<input type="text" name="code" required lay-verify="required"
					placeholder="请输入账号" autocomplete="off" class="layui-input">
			</div>

		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">密码</label>
			<div class="layui-input-inline">
				<input type="text" name="pass" required lay-verify="required"
					placeholder="请输入密码" autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">姓名</label>
			<div class="layui-input-inline">
				<input type="text" name="name" required lay-verify="required"
					placeholder="请输入姓名" autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item" style="margin-left: 50px;">
			<img style="margin-top: 4px;" src="${pageContext.request.contextPath}/AuthCodeServlet"
			onclick="this.src='${pageContext.request.contextPath}/AuthCodeServlet?'+Math.random();"/>
			<div class="layui-input-inline">
			<input type="text" name="Vcode" required lay-verify="required"
					placeholder="请输入验证码" autocomplete="off" class="layui-input">
			</div>
		</div>
		<input type="hidden" name='action' value='reg'/> 
		<div class="layui-form-item" style="margin-left: 50px">
<!-- 			<div class="layui-input-block"> -->
				<input type="submit" id="reg" value="注册" class="layui-btn" lay-submit lay-filter="reg"/>
				<input type="reset"  class="layui-btn" value="重置" /> 
				<input type="button" id="retlogin" onclick="tologin()" value="返回登录" class="layui-btn" />
				
<!-- 			</div> -->
		</div>
</form>
</fieldset>
</div>


<script type="text/javascript">

function tologin(){
	location.href="login.jsp"
}

formSubmit('/EmployeeServlet','submit(reg)','text',
	function(data){
			if(data==1){
			layer.msg("注册成功")	
		}else if(data=="repeat"){
			layer.msg("注册失败，用户已存在")
		}
		})
	
</script>



</body>
</html>