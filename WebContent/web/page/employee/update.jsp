<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="/web/header.jsp"%>
<title>修改员工信息</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title"
					style="marhin: 10px; padding: 5px;">
					<legend>信息维护-修改信息</legend>
					<form class="layui-form" lay-filter='formu' style="margin-left: 200px; margin-top: 80px;">
						 <div class="layui-form-item">
							<label class="layui-form-label">*员工账号</label>
							<div class="layui-input-inline">
								<input type="text" name="code" readonly="true"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							 <div class="layui-form-item">
							<label class="layui-form-label">姓名</label>
							<div class="layui-input-inline">
								<input type="text" name="name"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item">
							<label class="layui-form-label">所属部门</label>
							<div class="layui-input-inline">
								<input type="text" name="codeDept"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item">
							<label class="layui-form-label">电话</label>
							<div class="layui-input-inline">
								<input type="text" name="tel"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item" style="margin-left: 50px;">
							<span> 
							<input type="button" id='upd' value="确认" class="layui-btn" lay-submit lay-filter="upd" /> 
								<input type="reset" class="layui-btn" value="重置" />
								 <input type="button" value="取消" onclick='cencel()' class="layui-btn" />
							</span>
							</div>
						<input type='hidden' name='action' value='upd' />
					</form>
</fieldset>
<script type="text/javascript">
var code = '<%=request.getParameter("code")%>';
init()
initSel()
function init(){
	ajax("/EmployeeServlet",{action:"get",code:code},'json',function(data){
		console.log(data)
		form.val("formu",{code:data.code,name:data.name,codeDept:data.codeDept,tel:data.tel})
	})
}
function initSel(){
	ajax("/EmployeeServlet",{action:"list"},'json',function(data){
		console.log(data)
		var opt = "<option value=''>请选择部门</option>"
		$.each(data,function(){
			opt+="<option value='"+this.code+"'>"+this.name+"</option>"
		})
		$("select[name='codeDept']").html(opt);
		form.render();
	})
}
formSubmit("/EmployeeServlet", "submit(upd)", "text", function(data) {
	console.log(data)
	if (data == 1) {
		layer.msg("修改成功")
	} else {
//			console.log(data)
		layer.msg("修改失败")
	}

})		
		function cencel() {
			var index = parent.layer.getFrameIndex(window.name);
			parent.layer.close(index);
		}
	</script>
</body>
</html>