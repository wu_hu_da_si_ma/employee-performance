<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp"%>
<title>上传头像</title>
</head>
<body>
	<fieldset class="layui-elem-field layui-field-title"
		style="margin: 10px; padding: 15px;">
		<legend>图片列表</legend>
		<button type="button" class="layui-btn" id="test1">
			<i class="layui-icon">&#xe67c;</i>修改头像
		</button>
		<div class="layui-upload-list">
			<table class="layui-table">
				<thead>
					<tr>
						<th>文件名</th>
						<th>头像</th>
					</tr>
				<tbody id="picList"></tbody>
			</table>
		</div>
	</fieldset>
	<script type="text/javascript">

var code = '<%=request.getParameter("code")%>';
		var upload = layui.upload;
		//按钮submit监听
		//按纽upload渲染点击执行
		layui.use('upload', randerUpload('#test1',
				'/st_company/EmployeeUploadServlet', {
					code : code
				}, function(res) {
					console.log(res)
					if (res.code == 1) {
						layer.msg("上传成功")
						showPic(res.image)
					} else {
						layer.msg("上传失败")
					}

				}))
		init();
		function init(){
			ajax('/EmployeeServlet',{action:'get',code:code},'json',function(data){
				var image=data.image;
				if(image)showPic(image)
				else $("#picList").html('');
				
			})
		}
		function showPic(image) {
			console.log(image)
			var html = "<tr><td>"
					+ image
					+ "</td><td><img src='/image/"+image+"'class='layui-upload-img'></td>"
					
			$("#picList").html(html);
		}
	</script>

</body>
</html>