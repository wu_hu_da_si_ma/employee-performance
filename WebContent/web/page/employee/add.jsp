<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp"%>
<meta charset="UTF-8">
<title>添加员工</title>
</head>
<body>
	<fieldset class="layui-elem-field layui-field-title"
		style="marhin: 10px; padding: 5px;">
		<legend>信息维护-添加信息</legend>
		<form class="layui-form" style="margin-left: 200px; margin-top: 20px;">
			<div class="layui-form-item">
				<label class="layui-form-label">*员工账号</label>
				<div class="layui-input-inline">
					<input type="text" name="code" placeholder="请输入员工账号" required
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">员工名字</label>
				<div class="layui-input-inline">
					<input type="text" name="name" placeholder="请输入员工名字"
						autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">所属部门</label>
				<div class="layui-input-inline">
					<input type="text" name="codeDept" placeholder="请输入所属部门"
						autocomplete="off" class="layui-input">  
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">电话</label>
				<div class="layui-input-inline">
					<input type="text" name="tel" placeholder="请输入电话号码"
						autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item" style="margin-left: 50px;">
				<span> <input type="button" id='add' value="确认"
					class="layui-btn" lay-submit lay-filter="add" /> <input
					type="reset" class="layui-btn" value="重置" /> <input type="button"
					value="取消" onclick='cencel()' class="layui-btn" />
				</span>
			</div>
			<input type='hidden' name='action' value='add' />
		</form>
	</fieldset>
	<script type="text/javascript">
		formSubmit("/EmployeeServlet", "submit(add)", "text", function(data) {
			console.log(data)
			if (data == 1) {
				layer.msg("添加成功")
				// 		cencel()
			} else if (data == 'repeat') {
				console.log(data)
				layer.msg("部门已存在")
			} else {
// 				console.log(data)
				layer.msg("添加失败")
			}

		})
		function cencel() {
			var index = parent.layer.getFrameIndex(window.name);
			parent.layer.close(index);
		}
	</script>

</body>
</html>