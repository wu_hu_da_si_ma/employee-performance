<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="/web/header.jsp"%>
<title>修改员工信息</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title"
					style="marhin: 10px; padding: 5px;">
					<legend>信息维护-修改密码</legend>
					<form class="layui-form" lay-filter='formu' style="margin-left: 200px; margin-top: 80px;">
						 <div class="layui-form-item">
							<label class="layui-form-label">*员工账号</label>
							<div class="layui-input-inline">
								<input type="text" name="code" readonly="true"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							 <div class="layui-form-item">
							<label class="layui-form-label">原密码</label>
							<div class="layui-input-inline">
								<input type="text" name="oldPass"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item">
							<label class="layui-form-label">新密码</label>
							<div class="layui-input-inline">
								<input type="text" name="newPass"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item" style="margin-left: 50px;">
							<span> 
							<input type="button" id='revisepass' value="确认" class="layui-btn" lay-submit lay-filter="revisepass" /> 
								<input type="reset" class="layui-btn" value="重置" />
								 <input type="button" value="取消" onclick='cencel()' class="layui-btn" />
							</span>
							</div>
						<input type='hidden' name='action' value='revisepass' />
					</form>
</fieldset>
<script type="text/javascript">
var code = '<%=request.getParameter("code")%>';
init()
function init(){
	ajax("/EmployeeServlet",{action:"get",code:code},'json',function(data){
		console.log(data)
		form.val("formu",{code:data.code,name:data.name,codeDept:data.codeDept,tel:data.tel})
	})
}
formSubmit("/EmployeeServlet", "submit(revisepass)", "text", function(data) {
	console.log(data)
	if (data == 1) {
		layer.msg("修改成功")
		cencel();
	} else if(data==0){
//			console.log(data)
		layer.msg("密码错误")
	} else {
		layer.msg("修改失败")
	}

})		
		function cencel() {
			var index = parent.layer.getFrameIndex(window.name);
			parent.layer.close(index);
		}
	</script>
</body>
</html>