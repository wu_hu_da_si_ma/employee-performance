<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp"%>
<meta charset="UTF-8">
<title>部门信息维护</title>
</head>
<body>
<!-- http://localhost:8080/st_company/web/page/department/DepartmentList.jsp -->
	<div class="layui-collapse">
		<div class="layui-colla-item">
			<h2 class="layui-colla-title">员工绩效查询</h2>
			<div class="layui-colla-content layui-show">
				<fieldset class="layui-elem-field layui-field-title"
					style="marhin-top: 0px; padding: 5px">
					<legend>绩效信息-查询条件</legend>
					<form class="layui-form">
						<div class="layui-form-item">
							<label class="layui-form-label">员工编号</label>
							<div class="layui-input-inline">
								<input type="text" name="codeEmp" placeholder="请输入员工编号"
									autocomplete="off" class="layui-input">
							</div>
							<label class="layui-form-label">部门名称</label>
							<div class="layui-input-inline">
								<input type="text" name="codePro" placeholder="请输入项目编号"
									autocomplete="off" class="layui-input">
							</div>
							<span> <input type="button" id='search' value="查询" class="layui-btn"
								lay-submit lay-filter="search" /> <input type="reset"
								class="layui-btn" value="重置" /> <input type="button"
							 value="添加" onclick='openadd()' class="layui-btn" />
							</span>
						</div>
<!-- 						<input type='hidden' name='action' value='list' /> -->
										<input type='hidden' name = 'action' value='page'/>
										<input type='hidden' name = 'pageIndex' value='1'/>
										<input type='hidden' name = 'pageLimit' value='10'/>
					</form>
					</filedset>
			</div>
		</div>
		<div>
			<table class="layui-table">
				<colgroup>
					<col width="10px">
					<col width="15px">
					<col width="15px">
					<col width="15px">
					<col width="15px">
					<col width="15px">
					<col width="45px">
					<col>
				</colgroup>
				<thead>
					<tr>
						<th>序号</th>
						<th>员工编号</th>
						<th>员工姓名</th>
						<th>项目编号</th>
						<th>项目名称</th>
						<th>绩效分数</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody id="user_tbody"></tbody>
			</table>
			<div id="pageInfoUser" style="text-align: right; padding-right: 30px"></div>
		</div>
	</div>

	<script>
		element.render();
		formSubmit("/ScoreServlet", "submit(search)", "json", function(data) {
			console.log(data)
			var laypage = layui.laypage;
			var curr = $("input[name='pageIndex']").val();
			var limit = $("input[name='pageLimit']").val();
			setPageInfo('pageInfoUser',data.count,curr,limit,function (obj,first){
				console.log(obj)
				$("input[name='pageIndex']").val(obj.curr);
				$("input[name='pageLimit']").val(obj.limit);
				
				if(!first){
					refresh()
				}
			});
			
		var html="";
		$.each(data.list,function(i,dom){
			var nameEmp = dom.nameEmp?dom.nameEmp:"";
			var namePro = dom.namePro?dom.namePro:"";
			var score = dom.score?dom.score:"";
			var d = {i:i+1,codeEmp:dom.codeEmp,nameEmp:nameEmp,codePro:dom.codePro,namePro:namePro,score:score}
			var laytpl = layui.laytpl;
		 	html+=laytpl($("#tradd").html()).render(d);
		})
		$("#user_tbody").html(html);
		layer.msg("查询成功🦾");
		});
		refresh();
		function refresh(){
			$("#search").click();
		}
		
		function openadd(){
		openLayer("/web/page/score/add.jsp")
		}
		
		function del(codeEmp,codePro){
			openConfirm(function(){
				ajax("/ScoreServlet", {action:'del',codeEmp:codeEmp,codePro:codePro}, "text", function(data){
					console.log(data)
					if("exist"==data){
						layer.msg("删除失败--该部门存在员工")
					}else if(1==data){
						layer.msg("删除成功",refresh())
					}
				})
				
				layer.msg("删除成功❌");
			},"确认删除❓")
		}
		
		function openupd(codeEmp,codePro){
		openLayer("/web/page/score/update.jsp?codeEmp="+codeEmp+"&codePro="+codePro);
		}
	</script>
	<script type="text/html" id="tradd">
	<tr>
		<td>{{d.i}}</td>
		<td>{{d.codeEmp}}</td>
		<td>{{d.nameEmp}}</td>
		<td>{{d.codePro}}</td>
		<td>{{d.namePro}}</td>
		<td>{{d.score}}</td>
		<td>
		<a href='javascript:del("{{d.codeEmp}}","{{d.codePro}}");' class='layui-btn layui-btn-sm'>
		<i class ='layui-icon layui-icon-delete'></i>	
		</a>&nbsp;&nbsp;
		<input type='button' onclick="openupd('{{d.codeEmp}}','{{d.codePro}}')"class='layui-btn layui-btn-sm' value ='修改绩效'>
		</td>
	</tr>
	</script>
</body>
</html>