<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="/web/header.jsp"%>
<title>Insert title here</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title"
					style="marhin: 10px; padding: 5px;">
					<legend>信息维护-修改信息</legend>
					<form class="layui-form" lay-filter='formu' style="margin-left: 200px; margin-top: 80px;">
						<div class="layui-form-item">
							<label class="layui-form-label">员工编号</label>
							<div class="layui-input-inline">
								<input type="text" name="codeEmp" readonly='true'
									autocomplete="off" class="layui-input">
							</div>
							</div>
						 <div class="layui-form-item">
							<label class="layui-form-label">项目编号</label>
							<div class="layui-input-inline">
								<input type="text" name="codePro"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item">
							<label class="layui-form-label">绩效</label>
							<div class="layui-input-inline">
								<input type="text" name="score"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item" style="margin-left: 50px;">
							<span> 
							<input type="button" id='upd' value="确认" class="layui-btn" lay-submit lay-filter="upd" /> 
								<input type="reset" class="layui-btn" value="重置" />
								 <input type="button" value="取消" onclick='cencel()' class="layui-btn" />
							</span>
							</div>
						<input type='hidden' name='action' value='upd' />
					</form>
</fieldset>
<script type="text/javascript">
var codeEmp = '<%=request.getParameter("codeEmp")%>';
var codePro = '<%=request.getParameter("codePro")%>';
init()
function init(){
	ajax("/ScoreServlet",{action:"get",codeEmp:codeEmp,codePro:codePro},'json',function(data){
		console.log(data)
		form.val("formu",{codeEmp:data.codeEmp,codePro:data.codePro,score:data.score})
	})
}
formSubmit("/ScoreServlet", "submit(upd)", "text", function(data) {
	console.log(data)
	if (data == 1) {
		layer.msg("修改成功")
	} else if (data == 'repeat') {
		console.log(data)
		layer.msg("修改失败,绩效已存在")
	} else {
		layer.msg("修改失败")
	}

})		
		function cencel() {
			var index = parent.layer.getFrameIndex(window.name);
			parent.layer.close(index);
		}
	</script>
</body>
</html>