<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<meta charset="UTF-8">
<title>主界面</title>
</head>
<body>
<!-- http://localhost:8080/st_company/web/page/main.jsp -->
<body>
<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
    <div class="layui-logo layui-hide-xs layui-bg-black">目录</div>
    <!-- 头部区域（可配合layui 已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
      <!-- 移动端显示 -->
      <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-header-event="menuLeft">
        <i class="layui-icon layui-icon-spread-left"></i>
      </li>
      <li class="layui-nav-item">
        <a href="javascript:;">音乐</a>
        <dl class="layui-nav-child">
           <dd><a href="#">周杰伦-半岛铁盒</a></dd>
		  <audio autoplay controls src="../base/music/周杰伦-半岛铁盒.flac"></audio>
        </dl>
      </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item layui-hide layui-show-md-inline-block">
        <a href="javascript:;">
          <img src="/image/${user.image}" style="height:40px;" >
       	  用户  [${user.name}]
        </a>
        <dl class="layui-nav-child">
          <dd><a href="javascript:openupd();">修改资料</a></dd>
          <dd><a href="javascript:revisePass();">修改密码</a></dd>
          <dd><a href="${pageContext.request.contextPath}/EmployeeServlet?action=quit">退出登录</a></dd>
        </dl>
      </li>
    </ul>
  </div>
  
  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree" lay-filter="test">
        <li class="layui-nav-item layui-nav-itemed">
          <a class="" href="javascript:;">信息维护</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('./department/DepartmentList.jsp');">部门信息维护</a></dd>
            <dd><a href="javascript:openUrl('./employee/EmployeeList.jsp');">员工信息维护</a></dd>
            <dd><a href="javascript:openUrl('./protect/ProtectList.jsp');">项目信息维护</a></dd>
            <dd><a href="javascript:openUrl('./score/ScoreList.jsp');">绩效信息维护</a></dd>
          </dl>
        </li>
       
      </ul>
    </div>
  </div>
  
  <div class="layui-body">
   <!-- 内容主体区域 -->
   <iframe src="./welcome.jsp" name="rightframe" width="100%" height="100%"></iframe>
	
  </div>
  
  <div class="layui-footer" style="text-align: center;">
    <!-- 底部固定区域 -->
    @LL
  </div>
</div>
<script>
var code = "${user.code}"

function revisePass(){
	openLayer("/web/page/employee/revisePass.jsp?code="+code)
}
function openupd(){
	openLayer("/web/page/employee/update.jsp?code="+code)
	}
function openUrl(url){
	window.open(url,"rightframe")
}
layui.use(['element', 'layer', 'util'], function(){
  var element = layui.element
  ,layer = layui.layer
  ,util = layui.util
  ,$ = layui.$;
  
  //头部事件
  util.event('lay-header-event', {
    //左侧菜单事件
    menuLeft: function(othis){
      layer.msg('展开左侧菜单的操作', {icon: 0});
    }
    ,menuRight: function(){
      layer.open({
        type: 1
        ,content: '<div style="padding: 15px;"></div>'
        ,area: ['260px', '100%']
        ,offset: 'rt' //右上角
        ,anim: 5
        ,shadeClose: true
      });
    }
  });
  
});


</script>
</body>
</html>