<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp"%>
<meta charset="UTF-8">
<title>项目添加</title>
</head>
<body>
	<fieldset class="layui-elem-field layui-field-title"
		style="marhin: 10px; padding: 5px;">
		<legend>信息维护-添加信息</legend>
		<form class="layui-form" style="margin-left: 200px; margin-top: 50px;">
			<div class="layui-form-item">
				<label class="layui-form-label">*项目编号</label>
				<div class="layui-input-inline">
					<input type="text" name="code" placeholder="请输入项目编号" required
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">项目名</label>
				<div class="layui-input-inline">
					<input type="text" name="name" placeholder="请输入项目名"
						autocomplete="off" class="layui-input">
				</div>
			</div>
			
			
			<div class="layui-form-item" style="margin-left: 50px;">
				<span> <input type="button" id='add' value="确认"
					class="layui-btn" lay-submit lay-filter="add" /> <input
					type="reset" class="layui-btn" value="重置" /> <input type="button"
					value="取消" onclick='cencel()' class="layui-btn" />
				</span>
			</div>
			<input type='hidden' name='action' value='add' />
		</form>
	</fieldset>
	<script type="text/javascript">
		formSubmit("/ProjectServlet", "submit(add)", "text", function(data) {
			console.log(data)
			if (data == 1) {
				layer.msg("添加成功")
				// 		cencel()
			} else if (data == 'repeat') {
				console.log(data)
				layer.msg("项目已存在")
			} else {
// 				console.log(data)
				layer.msg("添加失败")
			}

		})
		function cencel() {
			var index = parent.layer.getFrameIndex(window.name);
			parent.layer.close(index);
		}
	</script>

</body>
</html>