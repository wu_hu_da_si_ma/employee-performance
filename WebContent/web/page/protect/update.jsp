<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="/web/header.jsp"%>
<title>修改项目信息</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title"
					style="marhin: 10px; padding: 5px;">
					<legend>信息维护-修改信息</legend>
					<form class="layui-form" lay-filter='formu' style="margin-left: 200px; margin-top: 80px;">
						 <div class="layui-form-item">
							<label class="layui-form-label">*项目编号</label>
							<div class="layui-input-inline">
								<input type="text" name="code" readonly="true"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							 <div class="layui-form-item">
							<label class="layui-form-label">项目名</label>
							<div class="layui-input-inline">
								<input type="text" name="name"
									autocomplete="off" class="layui-input">
							</div>
							</div>
							<div class="layui-form-item"> 
							<label class="layui-form-label">开始时间</label>
						<div class="layui-input-inline"> 
								<input type="text" name="bdate" id="bdate" placeholder="请输入开始时间" 
								autocomplete="off" class="layui-input">
						</div> 
						</div> 
						<div class="layui-form-item">
					<label class="layui-form-label">结束时间</label> 
							<div class="layui-input-inline">
							<input type="text" name="edate" id="edate" placeholder="请输入结束时间"
								autocomplete="off" class="layui-input"> 
							</div>
							</div> 
							
							<div class="layui-form-item" style="margin-left: 50px;">
							<span> 
							<input type="button" id='upd' value="确认" class="layui-btn" lay-submit lay-filter="upd" /> 
								<input type="reset" class="layui-btn" value="重置" />
								 <input type="button" value="取消" onclick='cencel()' class="layui-btn" />
							</span>
							</div>
						<input type='hidden' name='action' value='upd' />
					</form>
</fieldset>
<script type="text/javascript">
element.render();
layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //执行一个laydate实例
	laydate.render({
	elem:'#bdate'
	});
	laydate.render({
	elem:'#edate'
	})
	});

var code = '<%=request.getParameter("code")%>';
init()
function init(){
	ajax("/ProjectServlet",{action:"get",code:code},'json',function(data){
		console.log(data)
		form.val("formu",{code:data.code,name:data.name,bdate:data.bdate,edate:data.edate})
	})
}
formSubmit("/ProjectServlet", "submit(upd)", "text", function(data) {
	console.log(data)
	if (data == 1) {
		layer.msg("修改成功")
	} else {
//			console.log(data)
		layer.msg("修改失败")
	}

})		
		function cencel() {
			var index = parent.layer.getFrameIndex(window.name);
			parent.layer.close(index);
		}
		

	</script>
</body>
</html>