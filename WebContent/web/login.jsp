<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="/web/header.jsp"%>
<title>用户登录</title>
</head>
<body style="background: #D8CEF6">
	<h1 style="text-align:center;font-weight:bold;">欢迎登录员工绩效管理系统！</h1>
	<div class="window">
		<fieldset class="layui-elem-field" style="marhin: 10px">
			<legend>登录</legend>
			<form class="layui-form " onsubmit="return false">
				<div class="layui-form-item">
					<label class="layui-form-label">账号</label>
					<div class="layui-input-inline">
						<input type="text" name="code" id='code' required lay-verify="required"
							placeholder="请输入账号" autocomplete="off" class="layui-input">
					</div>

				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">密码</label>
					<div class="layui-input-inline">
						<input type="text" name="pass" required lay-verify="required"
							placeholder="请输入密码" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item" style="margin-left: 50px;">
					<img style="margin-top: 4px;"
						src="${pageContext.request.contextPath}/AuthCodeServlet"
						onclick="this.src='${pageContext.request.contextPath}/AuthCodeServlet?'+Math.random();" />
					<div class="layui-input-inline">
						<input type="text" name="Vcode" required lay-verify="required"
							placeholder="请输入验证码" autocomplete="off" class="layui-input">
					</div>
				</div>
				<input type="hidden" name='action' value='login' />
				<div class="layui-form-item" style="margin-left: 50px">
					<input type="submit" value="登录" class="layui-btn" lay-submit
						lay-filter="login" /> <input type="reset" class="layui-btn"
						value="重置" /> <input type="button" id="retreg" onclick="toreg()"
						value="去注册" class="layui-btn" />
				</div>
			</form>
		</fieldset>
	</div>


	<script type="text/javascript">
		function toreg() {
			location.href = "reg.jsp"
		}

		formSubmit('/EmployeeServlet', 'submit(login)', 'text', function(data) {
			if (data == 0) {
				layer.msg("用户不存在")
			} else if (data == 1) {
				layer.msg("登录成功")
				location.href = "/st_company/web/page/main.jsp"
			} else if (data == 2) {
				layer.msg("账号或密码错误")
			}else if(data==3){
				layer.msg("验证码错误")
			}

		})
	</script>



</body>
</html>