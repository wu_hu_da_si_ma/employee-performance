package com.situ.company.employee.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.util.FmEmpty;
import com.situ.company.util.JDBCUtil;

public class EmployeeDao {
	public Integer insert(EmployeeModel model) {
		String sql = "insert into st_company.employee(code,name,pass,code_dept,tel) value(?,?,?,?,?)";
		return JDBCUtil.update(sql, model.getCode(), model.getName(),model.getPass(),model.getCodeDept(),model.getTel());
	}

	public Integer delete(EmployeeModel model) {
		
		String sql = "delete from employee where code=?";
		return JDBCUtil.update(sql, model.getCode());
	}

	public Integer update(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("update st_company.employee set id=id");
		List<Object> vals = new ArrayList<Object>();
		String name = model.getName();
		if(name!=null) {
			sql.append(",name = ?");
			vals.add(name);
		}
		
		String codeDept=model.getCodeDept();
		if(codeDept!=null) {
			sql.append(" ,code_dept = ? ");
			vals.add(codeDept);
		}
		
		String tel=model.getTel();
		if(tel!=null) {
			sql.append(" ,tel = ? ");
			vals.add(tel);
		}
		String image = model.getImage();
		if(image!=null) {
			sql.append(" ,image=? ");
			vals.add(image);
		}
		
		sql.append(" where code = ?");
		vals.add(model.getCode());
		
		System.out.println(vals.size());
		System.out.println("Dao:"+sql);
		System.out.println(model.toString());
		return JDBCUtil.update(sql.toString(), vals);
	}

	public EmployeeModel selectEmployeeModel(EmployeeModel model) {
		
		List<EmployeeModel> list = selectList(model);
		System.out.println(list.get(0).toString());
		if ( FmEmpty.isEmpty(list)) // 空list
			return null;
		else
			return list.get(0);
	}

	public List<EmployeeModel> selectList(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("select code, pass, name, code_dept,(select name from department where code = code_dept) nameDept, tel, image from employee where 1=1 ");
		List<Object> vals = appendWhere(sql, model);
		Map<String, String> fields = new HashMap<>();//映射
		fields.put("code", "code");
		fields.put("name", "name");
		fields.put("pass", "pass");
		fields.put("codeDept", "code_dept");
		fields.put("tel", "tel");
		fields.put("nameDept", "namedept");
		fields.put("image", "image");
		return JDBCUtil.query(sql.toString(), vals, EmployeeModel.class, fields);
	}

	private List<Object> appendWhere(StringBuffer sql, EmployeeModel model) {
		
		List<Object> vals = new ArrayList<Object>();
		String code = model.getCode();
		if (!FmEmpty.isEmpty(code)) {
			sql.append(" and code like ? ");
			vals.add(code);
		}
		String name = model.getName();
		if (!FmEmpty.isEmpty(name)) {
			sql.append(" and name like ? ");
			vals.add(name);
		}
		String codeDept = model.getCodeDept();
		if (!FmEmpty.isEmpty(codeDept)) {
			sql.append(" and code_dept like ? ");
			vals.add(codeDept);
		}

		return vals;

	}

	public int selectCount(EmployeeModel m) {
		StringBuffer sql = new StringBuffer("select count(1) from employee where 1=1 ");
		List<Object> vals = appendWhere(sql, m);
		return JDBCUtil.queryInt(sql.toString(), vals);
	}

	public Integer updatePass(EmployeeModel model) {
		String sql = "update st_company.employee set pass=? where code=?";
		return JDBCUtil.update(sql, model.getPass(), model.getCode());
		
	}
}
