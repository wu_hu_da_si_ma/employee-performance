package com.situ.company.employee.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.employee.service.EmployeeService;
import com.situ.company.util.FmEmpty;
import com.situ.company.util.FmtRequest;
import com.situ.company.util.MD5Util;

/**
 * Servlet implementation class EmployeeServlet
 */
@WebServlet("/EmployeeServlet")
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// 接受请求 获取参数 封装对象 调用方法 返回结果
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		System.out.println(action);
		Object result = "";
		switch (action) {
		case "reg":
			result = reg(request);
			break;
		case "login":
			result = login(request);
			break;
		case "quit":
			quit(request,response);
			break;
		case "list":
			result = list(request);
			break;
		case "page":
			result = page(request);
			break;
		case "add":
			result = add(request);
			break;
		case "del":
			result = del(request);
			break;
		case "upd":
			result = upd(request);
			break;
		case "get":
			result = get(request);
			break;
		case "resetPass":
			result = resetPass(request);
			break;
		case "revisepass":
			result = revisePass(request);
			break;
		default:
			break;
		}
//		System.out.println("返回值为："+result);
		FmtRequest.write(response.getWriter(), result);
	}

	

	private EmployeeService service = new EmployeeService();
	
	private void quit(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		try {
			response.sendRedirect("/st_company/web/login.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String revisePass(HttpServletRequest request) {
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("code", "code");
		fields.put("pass", "oldPass");
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class, fields);
		return service.revicePass(model,request.getParameter("newPass"));
	}

	private String resetPass(HttpServletRequest request) {
		EmployeeModel model = new EmployeeModel();
		model.setCode(request.getParameter("code"));
		return service.resetPass(model);
	}

	private Object page(HttpServletRequest request) {
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("name", "name");
		fields.put("code", "code");
		fields.put("codeDept", "codeDept");
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class, fields);
		String pageIndex = request.getParameter("pageIndex");
		String pageLimit = request.getParameter("pageLimit");

		model.setPageIndex(Integer.parseInt(pageIndex));
		model.setPageLimit(Integer.parseInt(pageLimit));
		model.setPageOn(true);
		System.out.println("servlet:"+model.toString());
		List<EmployeeModel> list = service.selectList(model);
		int count = service.selectCount(model);
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		return map;
	}

	private Object get(HttpServletRequest request) {
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);
		return service.selectModel(model);
	}

	private Object upd(HttpServletRequest request) {
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);
		System.out.println("Servlet" + model.toString());
		return service.updateModel(model) + "";
	}

	private Object del(HttpServletRequest request) {
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);

		return service.delete(model);
	}

	private String add(HttpServletRequest request) {
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);
		return service.insert(model);
	}

	private Object list(HttpServletRequest request) {
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);
		return service.selectList(model);
	}

	private String reg(HttpServletRequest request) {
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);
		System.out.println(model.toString());
		return service.insert(model);
	}

	private String login(HttpServletRequest request) {
		String vCode = request.getParameter("Vcode");
		Object rightCode = request.getSession().getAttribute("authCode");
		if (FmEmpty.isEmpty(vCode) || !vCode.equals(rightCode)) {
			return "3";
		}
		EmployeeModel model = FmtRequest.parseModel(request, EmployeeModel.class);
		String res = service.login(model);
		if ("1".equals(res)) {
			request.getSession().setAttribute("user", service.selectModel(model));
		}
		return res;

	}

}
