package com.situ.company.employee.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.employee.service.EmployeeService;
import com.situ.company.util.FmtRequest;
import com.situ.company.util.Upload;

/**
 * Servlet implementation class EmployeeUploadServlet
 */
@WebServlet("/EmployeeUploadServlet")
public class EmployeeUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeUploadServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		Map<String, Object> map =null;
		try {
			map=Upload.upload(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		List<String> list = (List<String>) map.get("list");
		String image = list.get(0);
		String code = (String) map.get("code");
		
		EmployeeModel model =new EmployeeModel();
		model.setCode(code);
		model.setImage(image);
		System.out.println(model.toString());
		String res=service.updateModel(model);
		
		Map<String,String> result =new HashMap<String, String>();
		result.put("code",res);
		result.put("image", image);
		
		FmtRequest.write(response.getWriter(),result);
		
	}
	private EmployeeService service = new EmployeeService();
	

}
