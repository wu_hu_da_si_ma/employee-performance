package com.situ.company.employee.service;

import java.util.List;

import com.situ.company.employee.dao.EmployeeDao;
import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.util.FmEmpty;
import com.situ.company.util.MD5Util;



public class EmployeeService {
	private EmployeeDao dao = new EmployeeDao();
	
	public String insert(EmployeeModel model) {
		EmployeeModel m = new EmployeeModel();
		m.setCode(model.getCode());
		if(selectModel(m)==null) {
			String pass = model.getPass();
			if(FmEmpty.isEmpty(pass)) {
				pass="123";
			}
			model.setPass(MD5Util.MD5(pass));
			Integer res = dao.insert(model);
			return res==null?null:res.toString();
		}
		return "repeat";
		
	}
	public String delete(EmployeeModel model) {
		return dao.delete(model).toString();
	}
	/**
	 * 登录功能
	 * 
	 * @param model
	 * @return 0=账号不存在 1=登陆成功 2=密码错误
	 */
	public String login(EmployeeModel model) {
		// TODO Auto-generated method stub
		EmployeeModel m0=selectModel(model);
		if(m0==null) {
			return "0";
		}
		String pass = model.getPass();
		pass = MD5Util.MD5(pass);
		return m0.getPass().equals(pass)?"1":"2";
	}
	public EmployeeModel selectModel(EmployeeModel model) {
		System.out.println("service"+model.toString());
		return dao.selectEmployeeModel(model);
	}
	public List<EmployeeModel> selectList(EmployeeModel model) {
		
		String code = model.getCode();
		model.setCode(code==null?"%%":("%"+code+"%"));//通配符模糊查询
		String name = model.getName();
		model.setName(name==null?"%%":"%"+name+"%");
		String codeDept=model.getCodeDept();
		model.setCodeDept("".equals(codeDept)?"%%":"%"+codeDept+"%");
		System.out.println("Service"+model.toString());
		 List<EmployeeModel> list = dao.selectList(model);
		return list;
	}
	public String updateModel(EmployeeModel model) {
		return dao.update(model)+"";
	}
	public int selectCount(EmployeeModel model) {
		EmployeeModel m = new EmployeeModel();
		String code = model.getCode();
		m.setCode(code==null?"%%":("%"+code+"%"));//通配符模糊查询
		String name = model.getName();
		m.setName(name==null?"%%":"%"+name+"%");
		return dao.selectCount(m);
	}
	public String resetPass(EmployeeModel model) {
		model.setPass(MD5Util.MD5("123"));
		return dao.updatePass(model)+"";
	}
	public String revicePass(EmployeeModel model,String newPass) {
		model.setPass(MD5Util.MD5(model.getPass()));
		if(selectModel(model)==null)
		return "0";
		else {
			model.setPass(MD5Util.MD5(newPass));
			return dao.updatePass(model)+"";
		}
		
	}
	
}
