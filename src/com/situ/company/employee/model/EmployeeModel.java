package com.situ.company.employee.model;

import com.situ.company.util.PagerModel;

public class EmployeeModel extends PagerModel {
	private String code;
	private String name;
	private String pass;
	private String sex;
	private String tel;
	private String codeDept;
	private String nameDept;
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public EmployeeModel(String code, String name, String pass) {
		super();
		this.code = code;
		this.name = name;
		this.pass = pass;
	}

	public String getNameDept() {
		return nameDept;
	}

	public void setNameDept(String nameDept) {
		this.nameDept = nameDept;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public EmployeeModel() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getCodeDept() {
		return codeDept;
	}

	public void setCodeDept(String codeDept) {
		this.codeDept = codeDept;
	}

	@Override
	public String toString() {
		return "EmployeeModel [code=" + code + ", name=" + name + ", pass=" + pass + ", sex=" + sex + ", tel=" + tel
				+ ", codeDept=" + codeDept + ", nameDept=" + nameDept + ", image=" + image + "]";
	}

	
}
