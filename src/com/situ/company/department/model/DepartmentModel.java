package com.situ.company.department.model;

import com.situ.company.util.PagerModel;

public class DepartmentModel extends PagerModel{
	private String code;
	private String name;
	private String tel;
	private Long count;
	
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	@Override
	public String toString() {
		return "DepartmentModel [code=" + code + ", name=" + name + ", tel=" + tel + "count="+count+"]";
	}
	
}
