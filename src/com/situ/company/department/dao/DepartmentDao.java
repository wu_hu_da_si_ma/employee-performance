package com.situ.company.department.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.Soundbank;

import org.junit.experimental.ParallelComputer;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.util.FmEmpty;
import com.situ.company.util.JDBCUtil;

public class DepartmentDao {

	public int insert(DepartmentModel model) {
		String sql = "insert into department(code,name,tel)value(?,?,?)";
		return JDBCUtil.update(sql, model.getCode(), model.getName(), model.getTel());
	}

	public Integer delete(DepartmentModel model) {
		String sql = "delete from department where code = ? ";
		return JDBCUtil.update(sql, model.getCode());
	}

	public int update(DepartmentModel model) {
		String sql = "update department set name=?,tel=? where code=?";
		return JDBCUtil.update(sql, model.getName(), model.getTel(), model.getCode());
	}

	public DepartmentModel selectDepartmentModel(DepartmentModel model) {
		List<DepartmentModel> selectList = selectList(model);
		if (FmEmpty.isEmpty(selectList))
			return null;
		else
			return selectList.get(0);
	}

	public List<DepartmentModel> selectList(DepartmentModel model) {
		StringBuffer sql = new StringBuffer("select code, name ,tel ,(select count(1) from employee where code_dept=department.code) count from department where 1=1 ");
		List<Object> vals = appendWhere(sql, model);
		Map<String, String> fields = new HashMap<>();
		System.out.println(sql.toString());
		System.out.println(model.toString());
		
		return JDBCUtil.query(sql.toString(), vals, DepartmentModel.class);
	}

	private List<Object> appendWhere(StringBuffer sql, DepartmentModel model) {
		List<Object> vals = new ArrayList<Object>();
		String code = model.getCode();
		if (!FmEmpty.isEmpty(code)) {
			sql.append(" and code like ? ");
			vals.add(code);
		}
		String name = model.getName();
		if (!FmEmpty.isEmpty(name)) {
			sql.append(" and name like ? ");
			vals.add(name);
		}
		String tel = model.getTel();
		if (!FmEmpty.isEmpty(tel)) {
			sql.append(" and tel like ? ");
			vals.add(tel);
		}
		if(model.isPageOn()) {
			sql.append(" limit ?,? ");
			vals.add(model.getRowStart());
			vals.add(model.getPageLimit());
		}
		return vals;

	}

	public Integer selectCount(DepartmentModel m) {
		StringBuffer sql = new StringBuffer("select count(1) from department where 1=1 ");
		List<Object> vals = appendWhere(sql, m);
//		System.out.println(sql.toString());
		return JDBCUtil.queryInt(sql.toString(), vals);
	}
}