package com.situ.company.department.service;

import java.util.List;

import com.situ.company.department.dao.DepartmentDao;
import com.situ.company.department.model.DepartmentModel;
import com.situ.company.employee.dao.EmployeeDao;
import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.util.FmEmpty;

public class DepartmentService  {
	private DepartmentDao dao = new DepartmentDao();
	private EmployeeDao employeeDao=new EmployeeDao();
	public List<DepartmentModel> selectList(DepartmentModel model) {
		String code = model.getCode();
		model.setCode(code==null?"%%":("%"+code+"%"));//通配符模糊查询
		String name = model.getName();
		model.setName(name==null?"%%":"%"+name+"%");
		return dao.selectList(model);
	}

	public String insert(DepartmentModel model) {
		if(selectModel(model)==null) {
			Integer res = dao.insert(model);
			return res==null?null:res.toString();
		}
		return "repeat";
	}
	
	
	public String delete(DepartmentModel model) {
		EmployeeModel m1=new EmployeeModel();
		m1.setCodeDept(model.getCode());
		List<EmployeeModel> list = employeeDao.selectList(m1);
		if(FmEmpty.isEmpty(list)) {
			Integer res = dao.delete(model);
			return res==null ? null : res.toString();
		}
		return "exist";
	}

	public int update(DepartmentModel model) {
		return dao.update(model);
	}

	public DepartmentModel selectModel(DepartmentModel model) {
		DepartmentModel model2 =new DepartmentModel();
		model2.setCode(model.getCode());
		return dao.selectDepartmentModel(model2);
	}

	public Integer selectCount(DepartmentModel model) {
		DepartmentModel m = new DepartmentModel();
		String code = model.getCode();
		m.setCode(code==null?"%%":("%"+code+"%"));//通配符模糊查询
		String name = model.getName();
		m.setName(name==null?"%%":"%"+name+"%");
		return dao.selectCount(m);
	}
	
}
