package com.situ.company.department.servlet;

import java.io.IOException;import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.department.service.DepartmentService;
import com.situ.company.util.FmtRequest;

/**
 * Servlet implementation class DepartmentServlet
 */
@WebServlet("/DepartmentServlet")
public class DepartmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DepartmentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Object result = "";
		System.out.println(request.getParameter("action"));
		switch (request.getParameter("action")) {
		case "page":
			result = page(request);
			break;
		case "add":
			result = add(request);
			break;
		case "get":
			result = get(request);
			break;
		case "upd":
			result = upd(request);
			break;
		case "del":
			result = del(request);
			break;
		default:
			break;
		}
		FmtRequest.write(response.getWriter(), result);
	}

	private DepartmentService service = new DepartmentService();

	private Object page(HttpServletRequest request) {
		Map<String,String> fields=new HashMap<>();
		fields.put("name", "name");
		fields.put("code", "code");
		DepartmentModel model = FmtRequest.parseModel(request, DepartmentModel.class,fields );
		String pageIndex = request.getParameter("pageIndex");
		String pageLimit = request.getParameter("pageLimit");
	
		model.setPageIndex(Integer.parseInt(pageIndex));
		model.setPageLimit(Integer.parseInt(pageLimit));
		model.setPageOn(true);
			System.out.println("Servlet:"+model.toString());
		List<DepartmentModel> list = service.selectList(model);
//		System.out.println("servlet"+list);
		int count =service.selectCount(model);
		Map<String, Object> map =new HashMap<>();
//		System.out.println(pageIndex+""+pageLimit + "  " +count);
		map.put("list", list);
		map.put("count",count);
		return map;

	}

	private DepartmentModel get(HttpServletRequest request) {
		DepartmentModel model = FmtRequest.parseModel(request, DepartmentModel.class);
		return service.selectModel(model);
	}

	private String del(HttpServletRequest request) {
		System.out.println("删除");
		DepartmentModel model = FmtRequest.parseModel(request, DepartmentModel.class);
		return service.delete(model) + "";
	}

	private String upd(HttpServletRequest request) {
		DepartmentModel model = FmtRequest.parseModel(request, DepartmentModel.class);
		return service.update(model) + "";
	}

	private String add(HttpServletRequest request) {
		DepartmentModel model = FmtRequest.parseModel(request, DepartmentModel.class);
		return service.insert(model);
	}

//	private Object list(HttpServletRequest request) {
//		DepartmentModel model = FmtRequest.parseModel(request, DepartmentModel.class);
//		return service.selectList(model);
//	}

}
