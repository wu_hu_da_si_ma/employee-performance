package com.situ.company.project.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.project.model.ProjectModel;
import com.situ.company.util.FmEmpty;
import com.situ.company.util.JDBCUtil;

public class ProjectDao {
	public Integer insert(ProjectModel model) {
		String sql = "INSERT INTO `st_company`.`project` (`code`, `name`, `begin_date`,`end_date`) VALUES (?, ?, ?, ?);";
		
		return JDBCUtil.update(sql, model.getCode(),model.getName(),model.getBdate(),model.getEdate());
	}
	
	public Integer delete(ProjectModel model) {
		String sql = "delete from project where code=?";
		return JDBCUtil.update(sql, model.getCode());
		
	}
	
	public Integer update(ProjectModel model) {
		String sql = "update st_company.project set name=?,begin_date=?,end_date=? where code=?";
		return JDBCUtil.update(sql, model.getName(), model.getBdate(), model.getEdate(), model.getCode());
	}
	
	public ProjectModel selectProjectModel(ProjectModel model) {
		System.out.println("Dao:"+model);
		List<ProjectModel> list = selectList(model);
		if ( FmEmpty.isEmpty(list)) // 空list
			return null;
		else
			return list.get(0);
	}

	public List<ProjectModel> selectList(ProjectModel model) {
		StringBuffer sql = new StringBuffer("select code, name, begin_date, end_date from project where 1=1 ");
		List<Object> vals = appendWhere(sql, model);
		Map<String, String> fields = new HashMap<>();//映射
		fields.put("code", "code");
		fields.put("name", "name");
		fields.put("bdate", "begin_date");
		fields.put("edate", "end_date");
		System.out.println("Dao"+model.toString());
		System.out.println("Dao"+sql.toString());
		return JDBCUtil.query(sql.toString(), vals, ProjectModel.class, fields);
	}

	private List<Object> appendWhere(StringBuffer sql, ProjectModel model) {
		List<Object> vals = new ArrayList<Object>();
		String code = model.getCode();
		if (!FmEmpty.isEmpty(code)) {
			sql.append(" and code like ? ");
			vals.add(code);
		}
		String name = model.getName();
		if (!FmEmpty.isEmpty(name)) {
			sql.append(" and name like ? ");
			vals.add(name);
		}
		Date bDate = model.getBdate();
		sql.append(" and begin_date>='"+bDate+"'");
		Date eDate = model.getEdate();
		sql.append(" and end_date<='"+eDate+"'");
		return vals;
	}
	public int selectCount(ProjectModel m) {
		StringBuffer sql = new StringBuffer("select count(1) from project where 1=1 ");
		List<Object> vals = appendWhere(sql, m);
		return JDBCUtil.queryInt(sql.toString(), vals);
	}

	

}
