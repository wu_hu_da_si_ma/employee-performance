package com.situ.company.project.model;

import java.sql.Date;

import com.situ.company.util.PagerModel;

public class ProjectModel extends PagerModel{
	private String code;
	private String name;
	private Date bdate;
	private Date edate;
	
	

	public ProjectModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ProjectModel(String code, String name, Date bdate, Date edate) {
		super();
		this.code = code;
		this.name = name;
		this.bdate = bdate;
		this.edate = edate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBdate() {
		return bdate;
	}

	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}

	public Date getEdate() {
		return edate;
	}

	public void setEdate(Date edate) {
		this.edate = edate;
	}

	@Override
	public String toString() {
		return "ProjectModel [code=" + code + ", name=" + name + ", bdate=" + bdate + ", edate=" + edate
				+ "]";
	}
	


}
