package com.situ.company.project.servlet;

import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.project.model.ProjectModel;
import com.situ.company.project.service.ProjectService;
import com.situ.company.util.DateFormat;
import com.situ.company.util.FmtRequest;

/**
 * Servlet implementation class ProjectServlet
 */
@WebServlet("/ProjectServlet")
public class ProjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object result="";
		System.out.println(request.getParameter("action"));
		switch(request.getParameter("action")) {
		case "page":
			result = page(request);
			break;
		case "add":
			result = add(request);
			break;
		case "get":
			result = get(request);
			break;
		case "upd":
			result = upd(request);
			break;
		case "del":
			result = del(request);
			break;
		default:
			break;
		}
		System.out.println("返回了："+result);
		FmtRequest.write(response.getWriter(), result);
		
	}

	private Object upd(HttpServletRequest request) {
		String code = request.getParameter("code");
		String name = request.getParameter("name");
	    Date bdate =DateFormat.dateFormate(request.getParameter("bdate"));
	    Date edate =DateFormat.dateFormate(request.getParameter("edate"));
	    ProjectModel model = new ProjectModel(code,name,bdate,edate);
		return service.update(model);
	}

	private Object del(HttpServletRequest request) {
		ProjectModel model = FmtRequest.parseModel(request, ProjectModel.class);	
		return service.delete(model);
	}

	private Object get(HttpServletRequest request) {
		ProjectModel model = FmtRequest.parseModel(request, ProjectModel.class);	
		return service.selectModel(model);
	}

	private Object add(HttpServletRequest request) {
		ProjectModel model = FmtRequest.parseModel(request, ProjectModel.class);	
		return service.insert(model);
	}
	private ProjectService service = new ProjectService();
	private Object page(HttpServletRequest request) {
		Map<String,String> fields=new HashMap<>();
		fields.put("name", "name");
		fields.put("code", "code");
		ProjectModel model = FmtRequest.parseModel(request, ProjectModel.class,fields );
		String pageIndex = request.getParameter("pageIndex");
		String pageLimit = request.getParameter("pageLimit");
	
		if(request.getParameter("bdate")!="") {
		 Date bdate =DateFormat.dateFormate(request.getParameter("bdate"));
		 model.setBdate(bdate);
		}
		if(request.getParameter("edate")!="") {
		 Date edate =DateFormat.dateFormate(request.getParameter("edate"));
		 model.setEdate(edate);
		}
		System.out.println("查询"+model.toString());
		model.setPageIndex(Integer.parseInt(pageIndex));
		model.setPageLimit(Integer.parseInt(pageLimit));
		model.setPageOn(true);

		List<ProjectModel> list = service.selectList(model);
		int count =service.selectCount(model);
		Map<String, Object> map =new HashMap<>();

		map.put("list", list);
		map.put("count",count);
		return map;
	}

}
