package com.situ.company.project.service;

import java.sql.Date;
import java.util.List;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.project.dao.ProjectDao;
import com.situ.company.project.model.ProjectModel;
import com.situ.company.score.dao.ScoreDao;
import com.situ.company.score.model.ScoreModel;
import com.situ.company.util.DateFormat;
import com.situ.company.util.FmEmpty;

public class ProjectService {
	private ProjectDao dao = new ProjectDao();
	public List<ProjectModel> selectList(ProjectModel model) {
		
		if(model.getBdate()!=null&&model.getEdate()!=null&&model.getBdate().after(model.getEdate())) {
			return null;
			}
		System.out.println("Service:"+model.toString());
		Date bDate = model.getBdate();
		model.setBdate(bDate==null?DateFormat.dateFormate("0001-01-01"):bDate);
		Date eDate = model.getEdate();
		model.setEdate(eDate==null?DateFormat.dateFormate("9999-12-31"):eDate);
		System.out.println("Service:"+model.toString());
		String code = model.getCode();
		model.setCode(code==null?"%%":("%"+code+"%"));//通配符模糊查询
		String name = model.getName();
		model.setName(name==null?"%%":"%"+name+"%");
		return dao.selectList(model);
	}
	
	public int selectCount(ProjectModel model) {
		ProjectModel m = new ProjectModel();
			Date bDate = model.getBdate();
			m.setBdate(bDate==null?DateFormat.dateFormate("0000-00-00"):bDate);
			Date eDate = model.getEdate();
			m.setEdate(eDate==null?DateFormat.dateFormate("9999-12-31"):eDate);
		
		String code = model.getCode();
		m.setCode(code==null?"%%":("%"+code+"%"));//通配符模糊查询
		String name = model.getName();
		m.setName(name==null?"%%":"%"+name+"%");
		return dao.selectCount(m);
	}
	public Object insert(ProjectModel model) {
		ProjectModel m = new ProjectModel();
		m.setCode(model.getCode());
		System.out.println("Service:"+model.toString());
		if(FmEmpty.isEmpty(dao.selectProjectModel(m))) {
			
		return dao.insert(model)+"";
		}else {
			return "repeat";
		}
	}
	private ScoreDao scoreDao = new  ScoreDao();
	public Object delete(ProjectModel model) {
		ScoreModel scoreModel = new ScoreModel();
		scoreModel.setCodePro(model.getCode());
		if(FmEmpty.isEmpty(scoreDao.selectScoreModel(scoreModel)))
		return dao.delete(model)+"";
		else {
			return "exist";
		}
	}
	public Object selectModel(ProjectModel model) {
		ProjectModel m= new ProjectModel();
		m.setCode(model.getCode());
		return dao.selectProjectModel(m);
	}
	
	public Object update(ProjectModel model) {
		
		return dao.update(model)+"";
	}
	
}
