package com.situ.company.score.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.situ.company.project.model.ProjectModel;
import com.situ.company.score.model.ScoreModel;
import com.situ.company.util.FmEmpty;
import com.situ.company.util.JDBCUtil;

public class ScoreDao {

	public List<ScoreModel> selectList(ScoreModel model) {
		StringBuffer sql = new StringBuffer(" SELECT " + 
				" code_emp," + 
				" (select employee.name from employee where code = code_emp) empName, " + 
				" code_pro, " + 
				" (select project.name from project where code = code_pro) proName, " + 
				" score.score " + 
				" FROM st_company.score  where 1=1 ");
		List<Object> vals = appendWhere(sql, model);
		Map<String, String> fields = new HashMap<>();//映射
		fields.put("codeEmp", "code_emp");
		fields.put("nameEmp","empName");
		fields.put("codePro","code_pro");
		fields.put("namePro","proName");
		fields.put("score","score");
		System.out.println("Dao"+model.toString());
		System.out.println("Dao"+sql.toString());
		List<ScoreModel> list = JDBCUtil.query(sql.toString(), vals, ScoreModel.class, fields);
		return list;
	}
	
	private List<Object> appendWhere(StringBuffer sql, ScoreModel model) {
		List<Object> vals = new ArrayList<Object>();
		String code_emp = model.getCodeEmp();
		if (!FmEmpty.isEmpty(code_emp)) {
			sql.append(" and code_emp like ? ");
			vals.add(code_emp);
		}
		String code_pro = model.getCodePro();
		if (!FmEmpty.isEmpty(code_pro)) {
			sql.append(" and code_pro like ? ");
			vals.add(code_pro);
		}
		

		return vals;
	}

	public Object selectScoreModel(ScoreModel model) {
		List<ScoreModel> list = selectList(model);
		if (FmEmpty.isEmpty(list))
			return null;
		else
			return list.get(0);
	}

	public Object insert(ScoreModel model) {
		String sql=" INSERT INTO `st_company`.`score` (`code_emp`, `code_pro`, `score`) VALUES (?, ?, ?);";
		
		return JDBCUtil.update(sql, model.getCodeEmp(),model.getCodePro(),model.getScore());
	}

	public Integer selectCount(ScoreModel m) {
		StringBuffer sql = new StringBuffer("select count(1) from score where 1=1 ");
		List<Object> vals = appendWhere(sql, m);
//		System.out.println(sql.toString());
		return JDBCUtil.queryInt(sql.toString(), vals);
	}

	public Object delete(ScoreModel model) {
		String sql = "DELETE FROM score WHERE code_emp = ? and code_pro=? ";
		
		return JDBCUtil.update(sql, model.getCodeEmp(),model.getCodePro());
	}

	public Object update(ScoreModel model) {
		String sql ="update score set code_Pro=?, score=? where code_emp=?";
		return JDBCUtil.update(sql,model.getCodePro(),model.getScore(), model.getCodeEmp());
	}
}
