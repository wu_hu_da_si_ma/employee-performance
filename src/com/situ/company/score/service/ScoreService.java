package com.situ.company.score.service;

import java.util.List;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.employee.dao.EmployeeDao;
import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.project.dao.ProjectDao;
import com.situ.company.project.model.ProjectModel;
import com.situ.company.score.dao.ScoreDao;
import com.situ.company.score.model.ScoreModel;
import com.situ.company.util.FmEmpty;

public class ScoreService {
	private ScoreDao dao = new ScoreDao();

	public List<ScoreModel> selectList(ScoreModel model) {
		String code = model.getCodeEmp();
		model.setCodeEmp(code == null ? "%%" : ("%" + code + "%"));// 通配符模糊查询
		String name = model.getCodePro();
		model.setCodePro(name == null ? "%%" : "%" + name + "%");
		
		return dao.selectList(model);
	}

	public Object selectModel(ScoreModel model) {
		ScoreModel m = new ScoreModel();
		m.setCodeEmp(model.getCodeEmp());
		m.setCodePro(model.getCodePro());
		return dao.selectScoreModel(m);
	}
	private EmployeeDao employeeDao=new EmployeeDao();
	private ProjectDao projectDao =new ProjectDao();
	public Object insert(ScoreModel model) {
		if(FmEmpty.isEmpty(selectModel(model))) {
			
		
		EmployeeModel empModel = new EmployeeModel();
		empModel.setCode(model.getCodeEmp());
		List<EmployeeModel> liste = employeeDao.selectList(empModel);
		if(FmEmpty.isEmpty(liste)) {
			return "2";
		}
		ProjectModel proModel = new ProjectModel();
		proModel.setCode(model.getCodePro());
		List<ProjectModel> listp = projectDao.selectList(proModel);
		if(FmEmpty.isEmpty(listp)) {
			return "3";
		}
		return dao.insert(model)+"";
		}else {
			return "repeat";
		}
	}

	public Integer selectCount(ScoreModel model) {
		ScoreModel m = new ScoreModel();
		String codeEmp=model.getCodeEmp();
		m.setCodeEmp(codeEmp==null?"%%":("%"+codeEmp+"%"));
		String codePro=model.getCodePro();
		m.setCodeEmp(codePro==null?"%%":("%"+codePro+"%"));
		return dao.selectCount(m);
	}

	public Object delete(ScoreModel model) {
		return dao.delete(model);
	}

	public Object update(ScoreModel model) {
		if(FmEmpty.isEmpty(selectModel(model)))
		return dao.update(model)+"";
		else 
			return "repeat";
	}

}
