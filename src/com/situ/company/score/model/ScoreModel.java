package com.situ.company.score.model;

import com.situ.company.util.PagerModel;

public class ScoreModel extends PagerModel {
	private String codeEmp;
	private String nameEmp;
	private String namePro;
	private String codePro;
	private String score;

	public ScoreModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNameEmp() {
		return nameEmp;
	}

	public void setNameEmp(String nameEmp) {
		this.nameEmp = nameEmp;
	}

	public String getNamePro() {
		return namePro;
	}

	public void setNamePro(String namePro) {
		this.namePro = namePro;
	}

	public String getCodeEmp() {
		return codeEmp;
	}

	public void setCodeEmp(String codeEmp) {
		this.codeEmp = codeEmp;
	}

	public String getCodePro() {
		return codePro;
	}

	public void setCodePro(String codePro) {
		this.codePro = codePro;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "ScoreModel [codeEmp=" + codeEmp + ", nameEmp=" + nameEmp + ", namePro=" + namePro + ", codePro="
				+ codePro + ", score=" + score + "]";
	}



}
