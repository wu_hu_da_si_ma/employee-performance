package com.situ.company.score.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.project.model.ProjectModel;
import com.situ.company.project.service.ProjectService;
import com.situ.company.score.model.ScoreModel;
import com.situ.company.score.service.ScoreService;
import com.situ.company.util.FmtRequest;

/**
 * Servlet implementation class ScoreServlet
 */
@WebServlet("/ScoreServlet")
public class ScoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScoreServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Object result = "";
		System.out.println("action:"+request.getParameter("action"));
		switch (request.getParameter("action")) {
		case "page":
			result = page(request);
			break;
		case "get":
			result = get(request);
			break;
		case "add":
			result = add(request);
			break;
		case "del":
			result = del(request);
			break;
		case "upd":
			result = upd(request);
			break;
		default:
			break;
		}
		System.out.println("result:"+result);
		FmtRequest.write(response.getWriter(), result);
	}
	
	private Object upd(HttpServletRequest request) {
		ScoreModel model = FmtRequest.parseModel(request, ScoreModel.class);
		return service.update(model);
		
	}

	private Object del(HttpServletRequest request) {
		
		ScoreModel model = FmtRequest.parseModel(request, ScoreModel.class);
		return service.delete(model);
	}

	private Object add(HttpServletRequest request) {
		ScoreModel model = FmtRequest.parseModel(request, ScoreModel.class);
		return service.insert(model);
	}

	private Object get(HttpServletRequest request) {
		ScoreModel model = FmtRequest.parseModel(request, ScoreModel.class);
		return service.selectModel(model);
	}

	private ScoreService service =new ScoreService();
	private Object page(HttpServletRequest request) {
		Map<String,String> fields=new HashMap<>();
		fields.put("codeEmp", "codeEmp");
		fields.put("codePro", "codePro");
		ScoreModel model = FmtRequest.parseModel(request, ScoreModel.class,fields );
		String pageIndex = request.getParameter("pageIndex");
		String pageLimit = request.getParameter("pageLimit");
	
		model.setPageIndex(Integer.parseInt(pageIndex));
		model.setPageLimit(Integer.parseInt(pageLimit));
		model.setPageOn(true);
		System.out.println("Servlet:"+model.toString());
		List<ScoreModel> list = service.selectList(model);
		int count =service.selectCount(model);
//		System.out.println("servlet:"+list.get(0).toString());
		Map<String, Object> map =new HashMap<>();
		map.put("list", list);
		map.put("count",count);
		return map;
	}

}
