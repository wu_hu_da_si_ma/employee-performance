package com.situ.company.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class UserFilter
 */
@WebFilter(urlPatterns = {"/web/page/*","/DepartmentServlet","/EmployeeServlet","/ObjectServlet","/ScoreServlet"})
public class UserFilter implements Filter {

 
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		Object user = req.getSession().getAttribute("user");
		String action = req.getParameter("action");
		System.out.println(user);
		if("reg".equals(action)||"login".equals(action)) {
			chain.doFilter(request, response);
		}
		if(user==null) {
				res.sendRedirect("/st_company/web/login.jsp");
		}else {
			chain.doFilter(request, response);
		}
	}

}
