package com.situ.company.util;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class DateFormat {
	
		 public static Date dateFormate(String strDate) {
		        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		        java.util.Date d = null;  
		        try {  
		            d = format.parse(strDate);  
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        }  
		        java.sql.Date date = new java.sql.Date(d.getTime());
				return date;  
			
		}
}
