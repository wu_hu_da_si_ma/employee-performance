package com.situ.company.util;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

public class FmtRequest {

	/**
	 * 根据请求情况的参数情况反射到实体类对象中
	 * 
	 * @param <T>   需要反射的实体类对象
	 * @param req   请求对象
	 * @param clazz 需要反射的实体类的描述
	 * @return
	 */
	public static <T> T parseModel(HttpServletRequest req, Class<T> clazz) {
		T obj = null;
		try {
			obj = clazz.newInstance();// 实例化
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
		Map<String, String[]> map = req.getParameterMap();
		for (Entry<String, String[]> entry : map.entrySet()) {
			String name = entry.getKey();
			if ("action".equals(name)||"Vcode".equals(name)||"pageIndex".equals(name)||"pageLimit".equals(name))
				continue;
			try {
				java.lang.reflect.Field field = clazz.getDeclaredField(name);
				field.setAccessible(true);
				field.set(obj, (Object)entry.getValue()[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return obj;
	}

	/**
	 * 根据映射关系生产实体类变量
	 * 
	 * @param <T>
	 * @param req
	 * @param clazz
	 * @param entry key=属性名value=参数名
	 * @return
	 */
	public static <T> T parseModel(HttpServletRequest req, Class<T> clazz, Map<String, String> fields) {
		T obj = null;
		try {
			obj = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Entry<String, String> entry : fields.entrySet()) {
			String name = entry.getKey();
			String val = req.getParameter(entry.getValue());
			try {
				java.lang.reflect.Field field = clazz.getDeclaredField(name);
				field.setAccessible(true);
				field.set(obj, val);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return obj;
	}

	public static void write(Writer wr, String val) {
		try {
			wr.write(val);
			wr.flush();
			wr.close();
			wr = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void write(Writer wr, Object val) {
		if (val instanceof Collection<?>)
			write(wr, new JSONArray((Collection<?>) val).toString());
		else if (val instanceof Map<?, ?>)
			write(wr, new JSONObject((Map<?, ?>) val).toString());
		else if (val instanceof String) {
			write(wr, val.toString());
		}else{
			write(wr, new JSONObject(val).toString());
		}
		
	}

	
}
