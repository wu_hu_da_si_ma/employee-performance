package com.situ.company.util;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

public class JDBCUtil {
	private static ResourceBundle rb = ResourceBundle.getBundle("jdbc");
	private static String user = rb.getString("root");
	private static String pass = rb.getString("pass");
	private static String url = rb.getString("parkurl");
	private static String driver = rb.getString("driver");

	static {
		try {
			Class.forName(driver);// 加载驱动
		} catch (Exception e) {

		}
	}

	/**
	 * 获得连接对象
	 **/
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, pass);
	}

	/**
	 * 执行DML
	 */
	public static Integer update(String sql, List<Object> vals) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < vals.size(); i++) {
				ps.setObject(i + 1, vals.get(i));
			}
			return ps.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(conn, ps);
		}
		return null;

	}

	public static Integer update(String sql, Object... vals) {
		return update(sql, Arrays.asList(vals));

	}
//	数组中元素类型是应用类型=>asList返回的list的元素就是数组中的每个元素
//	数组中元素类型是基本数据类型=>asList返回的list的元素仅有一个,即这个数组

	public static void query(String sql) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(conn, ps, rs);
		}
	}

	/**
	 * 关闭连接
	 * 
	 * @param conn
	 * @param ps
	 * @param rs
	 */
	public static void close(Connection conn, PreparedStatement ps, ResultSet rs) {

		try {
			if (conn != null) {
				conn.close();
				conn = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (rs != null) {
				rs.close();
				rs = null;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	/**
	 * 重载
	 * 
	 * @param conn
	 * @param ps
	 */
	public static void close(Connection conn, PreparedStatement ps) {
		close(conn, ps, null);
	}

	/**
	 * 执行DQL
	 * 
	 * @param <T>
	 * @param sql   执行的sql
	 * @param vals  执行的sql的参数
	 * @param clazz 返回集合中的元素类型的雷的描述
	 * @param filed key=属性名 value=字段名 数据库字段名与类属性名不同时用这个map映射
	 * @return
	 */
	public static <T> List<T> query(String sql, List<Object> vals, Class<T> clazz, Map<String, String> fields) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<T> result = new ArrayList<T>();
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < vals.size(); i++) {
				ps.setObject(i + 1, vals.get(i));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				T model = clazz.newInstance();
				for (Entry<String, String> entry : fields.entrySet()) {
					Field filed = clazz.getDeclaredField(entry.getKey());
					filed.setAccessible(true);
					filed.set(model, rs.getObject(entry.getValue()));
				}
				result.add(model);
			}
		} catch (SQLException | InstantiationException | IllegalAccessException | NoSuchFieldException
				| SecurityException e) {

			e.printStackTrace();
		} finally {
			close(conn, ps, rs);
		}
		return result;
	}

	/**
	 * @param <T>
	 * @param sql
	 * @param vals
	 * @param clazz 直接通过 f.getName得到私有属性
	 * @return
	 */
	public static <T> List<T> query(String sql, List<Object> vals, Class<T> clazz) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<T> result = new ArrayList<T>();
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < vals.size(); i++) {
				ps.setObject(i + 1, vals.get(i));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				T model = clazz.newInstance();
				Field[] fileds = clazz.getDeclaredFields();
				for (Field f : fileds) {
					f.setAccessible(true);
					f.set(model, rs.getObject(f.getName()));
				}
				result.add(model);
			}

		} catch (SQLException | InstantiationException | IllegalAccessException | SecurityException e) {

			e.printStackTrace();
		} finally {
			close(conn, ps, rs);
		}
		// System.out.println(result);

		return result;
	}

	public static Integer queryInt(String sql, List<Object> vals) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < vals.size(); i++) {
				ps.setObject(i + 1, vals.get(i));
			}
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getInt(1);
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(conn, ps, rs);
		}

		return null;
	}

}
