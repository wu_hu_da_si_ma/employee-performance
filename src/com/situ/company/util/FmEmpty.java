package com.situ.company.util;

import java.util.List;
import java.util.Map;

public class FmEmpty {
	public static boolean isEmpty(String str) {
		boolean isEmpty = false;
		if (str == null || str.trim().isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}

	public static boolean isEmpty(Object str) {
		boolean isEmpty = false;
		if (str == null || isEmpty(str.toString())) {
			isEmpty = true;
		}
		return isEmpty;
	}

	public static boolean isEmpty(Object[] array) {
		boolean isEmpty = false;
		if (array == null || array.length == 0) {
			isEmpty = true;
		}
		return isEmpty;
	}

	public static boolean isEmpty(List<?> array) {
		boolean isEmpty = false;
		if (array == null || array.size() == 0) {
			isEmpty = true;
		}
		return isEmpty;
	}

	public static boolean isEmpty(Map<?, ?> map) {
		boolean isEmpty = false;
		if (map == null || map.isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}

}
